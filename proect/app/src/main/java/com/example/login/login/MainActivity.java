package com.example.login.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {




    Button button;
    TextView textView;
    EditText EditLogin;
    EditText EditPassword;
    boolean FreeSp;
    boolean FreeSho;
    String   UserPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FreeSp = false;
        FreeSho = false;
        button = (Button) findViewById(R.id.signIn);
        button.setOnClickListener(this);

        textView = (TextView) findViewById(R.id.answer);

        EditLogin = (EditText) findViewById(R.id.editLogin);
        EditPassword = (EditText) findViewById(R.id.editPassword);




    }


    @Override
    public void onClick(View v) {

        UserPass = EditPassword.getText().toString();

//Определяем, заполнены ли "editPassword" и "editLogin"
        if (EditLogin.getText().length() == 0 || EditPassword.getText().length() == 0 ){
            TextView scoreText = (TextView) findViewById(R.id.answer);
            scoreText.setText("Не введены данные!!!");
            FreeSp = false;
        }else {
            FreeSp = true;
        }
//колво символов в пароле
        if (FreeSp == true && EditPassword.getText().length() <= 5) {
            TextView scoreText = (TextView) findViewById(R.id.answer);
            scoreText.setText("Пароль слишком изи!!!");
            FreeSho = false;
        } else {
            FreeSho = true;
        }



//переход
        switch (v.getId()) {
            case R.id.signIn:
//переход
                if (FreeSho == true && FreeSp == true) {
                    Intent intent = new Intent(this, MainActivity2.class);
                    intent.putExtra("password", EditPassword.getText().toString());
                    intent.putExtra("login", EditLogin.getText().toString());
                    startActivity(intent);
                }
                break;

        }
    }

}
